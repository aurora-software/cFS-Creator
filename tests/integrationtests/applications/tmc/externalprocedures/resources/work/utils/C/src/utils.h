
/* Header file for function Utils in C language
 * Generated by TASTE on 2022-07-06 14:50:13
 * Context Parameters present : NO
 * Provided interfaces : Stub
 * Required interfaces : 
 * User-defined properties for this function:
 * DO NOT EDIT THIS FILE, IT WILL BE OVERWRITTEN DURING THE BUILD
 */

#pragma once

#include "dataview-uniq.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __unix__
   #include <stdlib.h>
   #include <stdio.h>
#endif

void utils_startup(void);

/* Provided interfaces */
void utils_PI_Stub( const asn1SccMyInteger *, asn1SccMyInteger * );

/* Required interfaces */



#ifdef __cplusplus
}
#endif
