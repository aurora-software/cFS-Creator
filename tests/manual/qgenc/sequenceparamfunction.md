# Testing function with sequence type param

**1. Steps:**

* Open project `tests/manual/qgenc/projects/sequenceparam/sequenceparam.pro`
* Open the `interfaceview.xml` file
* Clean the project
* Right click on the interfaceview and select the "Generate/update code skeletons" option
* Build and run the project

Expected results:

* Project should succesfully build and run.
* It should display the program output in a loop.
* No "assertion failed" info should appear.
