/*
  Copyright (C) 2022 European Space Agency - <maxime.perrotin@esa.int>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "asn1componentsimport.h"

#include "asn1reader.h"
#include "asn1systemchecks.h"
#include "errorhub.h"
#include "implementationshandler.h"
#include "ivobject.h"

#include <QApplication>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QTemporaryDir>

namespace ive {
namespace cmd {

ASN1ComponentsImport::ASN1ComponentsImport(
        Asn1Acn::Asn1SystemChecks *asn1Checks, const QString &srcPath, const QString &destPath)
    : QObject()
    , m_asn1Checks(asn1Checks)
    , m_srcPath(srcPath)
    , m_destPath(destPath)
{
}

ASN1ComponentsImport::~ASN1ComponentsImport() { }

void ASN1ComponentsImport::setAsn1SystemChecks(Asn1Acn::Asn1SystemChecks *asn1Checks)
{
    m_asn1Checks = asn1Checks;
    m_destPath = m_asn1Checks->projectPath();
}

void ASN1ComponentsImport::redoSourceCloning(const ivm::IVObject *object)
{
    if (!object
            || !(object->type() == ivm::IVObject::Type::Function
                    || object->type() == ivm::IVObject::Type::FunctionType)) {
        return;
    }

    const QStringList objPath = ivm::IVObject::path(object);
    const QString rootName = objPath.isEmpty() ? object->title() : objPath.front();
    const QString subPath = relativePathForObject(object);
    const QString sourcePath = m_tempDir.isNull() ? m_srcPath + QDir::separator() + rootName : m_tempDir->path();
    const QDir sourceDir { sourcePath };
    const QDir targetDir { m_destPath };
    if (!shared::copyDir(sourceDir.filePath(subPath), targetDir.filePath(subPath))) {
        QMessageBox::critical(qApp->activeWindow(), tr("Implementations import"),
                tr("Critical issues during implementations import for object: %1").arg(object->titleUI()));
    }
}

void ASN1ComponentsImport::undoSourceCloning(const ivm::IVObject *object)
{
    if (!object
            || !(object->type() == ivm::IVObject::Type::Function
                    || object->type() == ivm::IVObject::Type::FunctionType)) {
        return;
    }

    const auto res = QMessageBox::question(qApp->activeWindow(), tr("Implementations import undo"),
            tr("Do you want to remove implementations files for object %1?").arg(object->titleUI()));
    if (res == QMessageBox::No) {
        return;
    }

    const QString subPath = relativePathForObject(object);
    const QString destPath = m_tempDir->path() + QDir::separator() + subPath;
    const QString sourcePath { m_destPath + QDir::separator() + subPath };
    if (!shared::moveDir(sourcePath, destPath)) {
        QMessageBox::critical(qApp->activeWindow(), tr("Implementations import undo"),
                tr("Critical issues during implementations import undo for object: %1").arg(object->titleUI()));
    }
}

void ASN1ComponentsImport::redoAsnFileImport(const ivm::IVObject *object)
{
    if (!object
            || !(object->type() == ivm::IVObject::Type::Function
                    || object->type() == ivm::IVObject::Type::FunctionType)) {
        return;
    }

    const QStringList objPath = ivm::IVObject::path(object);
    const QString rootName = objPath.isEmpty() ? object->title() : objPath.front();
    const QString sourcePrefix = m_tempDir.isNull() ? m_srcPath : m_tempDir->path();
    const QDir sourceDir { sourcePrefix + QDir::separator() + rootName };
    const QDir targetDir { m_destPath };

    QList<QFileInfo> asnFiles = sourceDir.entryInfoList(
            { QLatin1String("*.asn1"), QLatin1String("*.asn"), QLatin1String("*.acn") }, QDir::Files);

    QVector<QFileInfo> fileInfos;
    std::copy_if(asnFiles.begin(), asnFiles.end(), std::back_inserter(fileInfos), [targetDir](const QFileInfo &fi) {
        const QString destFilePath { targetDir.filePath(fi.fileName()) };
        return !shared::isSame(destFilePath, fi.absoluteFilePath());
    });

    auto alreadyExistingModules = asn1ModuleDuplication(m_asn1Checks, fileInfos);
    if (!alreadyExistingModules.isEmpty()) {
        auto res = QMessageBox::question(qApp->activeWindow(), tr("Import ASN1 files"),
                tr("%1 module(s) already exist(s) in project or have(s) same type assignment(s), "
                   "do you want add it/them anyway?")
                        .arg(alreadyExistingModules.join(", ")));
        if (res == QMessageBox::No) {
            return; // CHECK: probably skip only files with module duplication
        }
    }

    QStringList importedAsnFiles;
    for (const QFileInfo &file : qAsConst(fileInfos)) {
        QString destFilePath { targetDir.filePath(file.fileName()) };
        if (QFile::exists(destFilePath)) {
            auto res = QMessageBox::question(qApp->activeWindow(), tr("Import ASN1 files"),
                    tr("%1 already exists in project directory, do you want to rename importing one")
                            .arg(file.fileName()));
            if (res == QMessageBox::No) {
                continue;
            }
            bool ok;
            const QString text = QInputDialog::getText(qApp->activeWindow(), tr("Rename importing ASN1 file"),
                    tr("File name:"), QLineEdit::Normal, rootName + file.fileName(), &ok);
            if (!ok || !text.isEmpty())
                destFilePath = targetDir.filePath(text);
        }
        if (shared::copyFile(file.absoluteFilePath(), destFilePath)) {
            importedAsnFiles.append(destFilePath);
        } else {
            if (QFile::exists(destFilePath))
                importedAsnFiles.append(destFilePath);
            shared::ErrorHub::addError(shared::ErrorItem::Error, tr("%1 wasn't imported").arg(file.fileName()));
        }
    }
    if (!importedAsnFiles.isEmpty())
        m_importedAsnFiles << importedAsnFiles;

    Q_EMIT asn1FilesImported(importedAsnFiles);
}

void ASN1ComponentsImport::undoAsnFileImport()
{
    const auto res = QMessageBox::question(qApp->activeWindow(), tr("Import ASN1 files undo"),
            tr("Do you want to remove ASN1 files: %1?").arg(m_importedAsnFiles.join(QLatin1String(", "))));
    if (res == QMessageBox::No) {
        return;
    }

    for (const QString &filePath : qAsConst(m_importedAsnFiles)) {
        QFile::remove(filePath);
    }

    Q_EMIT asn1FilesRemoved(m_importedAsnFiles);
}

QString ASN1ComponentsImport::relativePathForObject(const ivm::IVObject *object) const
{
    return ive::kRootImplementationPath + QDir::separator() + object->title().toLower();
}

QStringList ASN1ComponentsImport::asn1ModuleDuplication(
        Asn1Acn::Asn1SystemChecks *asn1Checks, const QVector<QFileInfo> &asn1FileInfos)
{
    if (asn1FileInfos.empty()) {
        return {};
    }
    Asn1Acn::Asn1Reader parser;
    QStringList errorMessages;
    QStringList modulesNames;
    const std::map<QString, std::unique_ptr<Asn1Acn::File>> asn1Data =
            parser.parseAsn1Files(asn1FileInfos, &errorMessages);
    if (asn1Data.empty()) {
        QStringList filenames;
        for (const QFileInfo &fi : qAsConst(asn1FileInfos)) {
            filenames.append(fi.fileName());
        }
        shared::ErrorHub::addError(shared::ErrorItem::Error,
                QObject::tr("%1 was/were not imported: ").arg(filenames.join(", ")), errorMessages.join(", "));
        return modulesNames;
    }

    QStringList projectDefinitionsNames;
    QStringList projectTypeAssignmentsNames;
    const std::vector<Asn1Acn::Definitions *> &projectDefinitions = asn1Checks->definitionsList();
    for_each(projectDefinitions.cbegin(), projectDefinitions.cend(), [&](Asn1Acn::Definitions *defs) {
        projectDefinitionsNames << defs->name();
        projectTypeAssignmentsNames << defs->typeAssignmentNames();
    });

    for (auto it = asn1Data.cbegin(); it != asn1Data.cend(); ++it) {
        for (const auto &defs : it->second->definitionsList()) {
            if (projectDefinitionsNames.contains(defs->name())) {
                modulesNames.append(defs->name());
            } else {
                for (const QString &typeAssignment : defs->typeAssignmentNames()) {
                    if (projectTypeAssignmentsNames.contains(typeAssignment)) {
                        modulesNames.append(defs->name());
                    }
                }
            }
        }
    }
    return modulesNames;
}

} // namespace ive
} // namespace cmd
